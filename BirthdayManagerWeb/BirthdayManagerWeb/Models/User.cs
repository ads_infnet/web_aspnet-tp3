﻿using System;

namespace BirthdayManagerWeb.Controllers
{
    internal class User
    {
        public int UserID { get; internal set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime BirthDate { get; internal set; }
    }
}