﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BirthdayManagerWeb.Controllers
{
    public class EditController : Controller
    {
        static List<User> users = new List<User>();

        // GET: Edit
        public ActionResult Index()
        {
            return View();
        }

        // GET: Edit/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Edit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Edit/Create
        [HttpPost]
        public ActionResult Create(string txtName, string txtLastName, string dtBirthDate)
        {
            try
            {
                int userID = int.Parse(DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                string firstName = txtName;
                string secondName = txtLastName;
                DateTime birthDate;
                DateTime.TryParseExact(dtBirthDate, "dd/MM/yyyy", null, DateTimeStyles.None, out birthDate);


                users.Add(new User { UserID = userID, FirstName = firstName, SecondName = secondName, BirthDate = birthDate });

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: Edit/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Edit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Edit/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Edit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
